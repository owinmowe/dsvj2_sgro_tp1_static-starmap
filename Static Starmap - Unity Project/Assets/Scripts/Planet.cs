﻿using UnityEngine;

public class Planet : MonoBehaviour
{
    [SerializeField] PlanetData planetData;
    [SerializeField] Transform sunPosition;
    [SerializeField] GameObject virtualCamera;
    bool translationActivated = true;
    float currentAngle = 0;
    float currentRotation = 0;
    float planetCurrentSpeed = 0;
    float sunDistance = 0;
    // Start is called before the first frame update
    void Start()
    {
        sunDistance = Vector3.Distance(sunPosition.position, transform.position);
        planetCurrentSpeed = planetData.translationSpeed + UnityEngine.Random.Range(-planetData.speedVariance, planetData.speedVariance);
    }

    // Update is called once per frame
    void Update()
    {
        if (translationActivated)
        {
            Vector3 v3 = Vector3.zero;
            currentAngle += planetCurrentSpeed * Time.deltaTime;
            v3.x = sunDistance * Mathf.Cos(currentAngle);
            v3.z = sunDistance * Mathf.Sin(currentAngle);
            transform.position = v3;
        }
        currentRotation += planetData.rotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y + currentRotation, transform.rotation.z));
    }

    public int GetId()
    {
        return planetData.id;
    }

    public void SetCameraState(bool state)
    {
        virtualCamera.SetActive(state);
    }

    public void SetTranslationState(bool state)
    {
        translationActivated = state;
    }

}

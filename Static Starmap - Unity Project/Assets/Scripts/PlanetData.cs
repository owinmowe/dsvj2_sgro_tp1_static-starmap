﻿using UnityEngine;

[CreateAssetMenu(fileName = "Planet Data", menuName = "ScriptableObjects/Planet Data", order = 1)]
public class PlanetData : ScriptableObject
{
    [Header("Planet logic data")]
    public float translationSpeed = 10;
    public float speedVariance = .1f;
    public float rotationSpeed = 10;
    public int id;
}

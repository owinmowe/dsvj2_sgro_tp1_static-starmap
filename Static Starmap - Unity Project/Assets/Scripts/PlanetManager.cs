﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetManager : MonoBehaviour
{

    [SerializeField] Planet[] planets;
    [SerializeField] GameObject systemVirtualCamera;

    public void ActivatePlanetFirstView(int id)
    {
        DeactivateAllCameras();
        if(id == 0)
        {
            systemVirtualCamera.SetActive(true);
            foreach (Planet planet in planets)
            {
                planet.SetTranslationState(true);
            }
        }
        else
        {
            foreach (Planet planet in planets)
            {
                planet.SetTranslationState(false);
                if (planet.GetId() == id)
                {
                    planet.SetCameraState(true);
                }
            }
        }
    }

    private void DeactivateAllCameras()
    {
        systemVirtualCamera.SetActive(false);
        foreach (Planet planet in planets)
        {
            planet.SetCameraState(false);
        }
    }
}
